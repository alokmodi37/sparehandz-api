-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 14, 2017 at 10:54 AM
-- Server version: 5.6.37
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apphos7_sparehandz`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`apphos7_shandz`@`localhost` PROCEDURE `CalculateDistance` (IN `units` VARCHAR(10), IN `lat` VARCHAR(20), IN `lon` VARCHAR(20), IN `postType` VARCHAR(200), IN `currentUser` INT(10))  BEGIN
	SET @sql = CONCAT("
		   SELECT * FROM (	
		   SELECT posts.id, posts.latitude, posts.longitude, skills.skill as type, (CASE WHEN posts.user_id = ", currentUser ," THEN 1 ELSE 0 END) as  isMyPost,
			  ROUND((CASE '", units, "' WHEN 'miles' 
			     THEN 3959
			     ELSE 6371 
			   END * acos( cos( radians(", lat ,") ) 
			       * cos( radians(posts.latitude) ) 
			       * cos( radians(posts.longitude) - radians(", lon, ")) + sin(radians(", lat, ")) 
			       * sin( radians(posts.latitude)))
			  ), 3) AS distance
		   FROM posts INNER JOIN skills ON posts.service_sub_type = skills.uniqueID WHERE posts.is_active = 1 AND FIND_IN_SET(posts.service_type ,'", postType ,"') ) AS p WHERE p.distance <= 5
	   ");

	PREPARE stmt FROM @sql;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;

  END$$

CREATE DEFINER=`apphos7_shandz`@`localhost` PROCEDURE `SearchPost` (IN `units` VARCHAR(10), IN `lat` VARCHAR(20), IN `lon` VARCHAR(20), IN `postType` VARCHAR(200), IN `searchText` VARCHAR(200), IN `currentUser` INT(10))  BEGIN
	SET @sql = CONCAT("
		   SELECT * FROM (	
		   SELECT posts.id, posts.latitude, posts.longitude, skills.skill as type, (CASE WHEN posts.user_id = ", currentUser ," THEN 1 ELSE 0 END) as  isMyPost,
			  ROUND((CASE '", units, "' WHEN 'miles' 
			     THEN 3959
			     ELSE 6371 
			   END * acos( cos( radians(", lat ,") ) 
			       * cos( radians(posts.latitude) ) 
			       * cos( radians(posts.longitude) - radians(", lon, ")) + sin(radians(", lat, ")) 
			       * sin( radians(posts.latitude)))
			  ), 3) AS distance, skills.skill
		   FROM posts JOIN skills ON skills.uniqueID = posts.service_sub_type WHERE FIND_IN_SET(posts.service_type ,'", postType ,"') AND (posts.caption LIKE '%", searchText , "%' OR posts.description LIKE '%", searchText , "%'  OR skills.skill LIKE '%", searchText , "%' ) AND posts.is_active = 1 ) AS p  WHERE p.distance <= 5
	   ");
	PREPARE stmt FROM @sql;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
  END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', '$2y$10$WGDVcWyDWJx58FJaeHQDzebgQTFyP5nOUKixSkEmxoUeJ/CLmAfwO', 'YXQx8CZsGMTYqXotmB5kwDeig1Thk2weIf71UFgJlKU14Rvh9bxx8IUHNoIZ', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `applicants`
--

CREATE TABLE `applicants` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `applicants`
--

INSERT INTO `applicants` (`id`, `user_id`, `post_id`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 5, 5, 0, NULL, '2017-08-28 17:55:20'),
(2, 4, 6, 0, NULL, '2017-08-29 10:05:45'),
(3, 5, 7, 0, NULL, '2017-08-28 22:07:53'),
(4, 4, 8, 0, NULL, '2017-08-29 10:05:34'),
(5, 4, 10, 0, NULL, '2017-08-29 10:14:46'),
(6, 4, 11, 0, NULL, '2017-08-29 10:06:07'),
(7, 4, 12, 0, NULL, '2017-08-29 10:10:46'),
(8, 4, 13, 0, NULL, '2017-08-29 10:28:06'),
(9, 4, 14, 0, NULL, '2017-08-29 10:14:53'),
(10, 4, 15, 0, NULL, '2017-08-29 10:15:00'),
(11, 4, 16, 0, NULL, '2017-08-29 10:19:31'),
(12, 4, 17, 0, NULL, '2017-08-29 10:26:05'),
(13, 4, 18, 0, NULL, '2017-08-29 10:25:20'),
(14, 4, 19, 0, NULL, '2017-08-29 10:07:50'),
(15, 4, 20, 0, NULL, '2017-08-29 10:06:21'),
(16, 5, 21, 0, NULL, '2017-08-29 10:54:48'),
(17, 7, 25, 0, NULL, '2017-09-03 15:03:32'),
(18, 7, 26, 0, NULL, '2017-09-03 15:12:58'),
(19, 7, 24, 0, NULL, '2017-09-05 15:55:23'),
(20, 6, 27, 1, NULL, NULL),
(21, 11, 32, 0, NULL, '2017-09-11 12:05:07'),
(22, 12, 16, 0, NULL, '2017-09-14 13:49:29'),
(23, 12, 35, 1, NULL, NULL),
(24, 12, 36, 1, NULL, NULL),
(25, 12, 21, 1, NULL, NULL),
(26, 12, 11, 1, NULL, NULL),
(27, 12, 37, 1, NULL, NULL),
(28, 5, 38, 0, NULL, '2017-09-14 14:04:40');

-- --------------------------------------------------------

--
-- Table structure for table `chat_head`
--

CREATE TABLE `chat_head` (
  `id` int(10) UNSIGNED NOT NULL,
  `sender_one` int(10) DEFAULT NULL,
  `sender_two` int(10) DEFAULT NULL,
  `firebase_identifier` varchar(200) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `chat_head`
--

INSERT INTO `chat_head` (`id`, `sender_one`, `sender_two`, `firebase_identifier`) VALUES
(1, 4, 5, '71e771b7b888f72e4f5e6652caef2039'),
(2, 5, 5, 'd62a52d164513609da869e387acf70aa'),
(3, 6, 7, '29aa832ffdbe5cf95d45951b235e719b'),
(4, 10, 11, '18953675d0ae6dc716fcdcac691309fc'),
(5, 12, 5, 'd10c67d5f4c1934e61e59011814e0b2c');

-- --------------------------------------------------------

--
-- Table structure for table `connections`
--

CREATE TABLE `connections` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `post_user_id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `connections`
--

INSERT INTO `connections` (`id`, `user_id`, `post_user_id`, `post_id`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 5, 4, 5, 1, NULL, NULL),
(2, 5, 4, 7, 1, NULL, NULL),
(21, 5, 12, 38, 1, NULL, NULL),
(20, 12, 5, 16, 1, NULL, NULL),
(6, 4, 5, 20, 1, NULL, NULL),
(7, 4, 5, 19, 1, NULL, NULL),
(8, 4, 5, 12, 1, NULL, NULL),
(9, 4, 5, 10, 1, NULL, NULL),
(10, 4, 5, 14, 1, NULL, NULL),
(11, 4, 5, 15, 1, NULL, NULL),
(12, 4, 5, 16, 1, NULL, NULL),
(13, 4, 5, 18, 1, NULL, NULL),
(14, 4, 5, 17, 1, NULL, NULL),
(15, 4, 5, 13, 1, NULL, NULL),
(16, 5, 4, 21, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_05_12_083947_create_skills_table', 1),
(4, '2017_05_12_084017_create_profile_table', 1),
(5, '2017_05_12_084050_create_setting_table', 1),
(6, '2017_05_12_084112_create_reviews_table', 1),
(7, '2017_05_12_084137_create_notifications_table', 1),
(8, '2017_05_12_094505_create_services_table', 1),
(9, '2017_05_12_094506_create_posts_table', 1),
(10, '2017_05_12_094507_create_post_images_table', 1),
(11, '2017_05_12_094508_create_applicants_table', 1),
(12, '2017_05_12_094509_create_connections_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `notification` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notification_token`
--

CREATE TABLE `notification_token` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) DEFAULT NULL,
  `firebase_token` longtext COLLATE utf8_bin,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `notification_token`
--

INSERT INTO `notification_token` (`id`, `user_id`, `firebase_token`, `updated_at`, `created_at`) VALUES
(1, 1, 'fiBDbGByzzI:APA91bE_lMsnrzwk7nY44IJ7wSAKFjqus0StFF_p787L-8iifDvP8havj22UJB_BoU2QmfNhAt2l7PnZC0LqoZE2G8MMw2elo3mhJdOOg7XO5rcCLMUGNQMzDGZv-T3DbAqtDFlUnn7F', NULL, NULL),
(2, 2, 'dvC1pns5w6U:APA91bHf5WsLlOCv8qz9bw533X1CBquuCaH9CKlMbc_iKAFrHORSAIsfYYqVcs29STORKhu9nyzZqKEy12UBq3Q5mgsulaU71uPYf0Td_tjJKfqf0lzdaS5u188yyuW2miVdIQ33kqg2', NULL, NULL),
(3, 3, 'eXumVTFmO0Q:APA91bGcs29EdtEtBbUOmFRwxLfEGu_bkYSbmr364-2c6AZmcbhkaHyNgtK13ndzl59Lp18eUrwZCciSH4ygd3pdB8Fwfqf9PCbpjQJIymqfozyDrgNtqI2hVv0zXE8BCtQ3BU4ZcXzN', NULL, NULL),
(4, 4, 'fYQgDbqtiQA:APA91bEg6eeWXN6potu4ZWk6GMdbda3IAgOKjup14IKHPnDBcqOg8yD0FzUPQ7y0CsF2iq9-wLuKPp10csFEdDyDnCkjIM7JUm3xrU1aLPdxxRVsz4W-GwNJLGNXFpyo5MxS-zB6dqxG', '2017-08-29 06:54:30', NULL),
(5, 5, 'f6lSUQJYeDw:APA91bFMLQDs2JQKv5a2OSUyqsToBDHIFWGyv5haKfUlRN0LbhHZgU0uRx2u64fHve4uG_IIhBWPj7MMI8w5g2pn5NvBW7pi5RXFaz90HILBgnmP0B4mXueI1-nCV6oAA1ZJGMuBZlMB', '2017-09-14 10:25:52', NULL),
(6, 6, 'eq8tMm6wmJw:APA91bGKz8gm1tS2UtLqYd2OFSgaq1V3O-LrJWcJcKcuSylPjltgIlyLJ7QkerfexY6AJURwJXSTwv-esWVNaD0yJzgw1AqA1ZpGXUHsad-bJM_tfKvQaTAqp8X0jVCtcmkf3R5YTOGA', '2017-09-05 09:50:04', NULL),
(7, 7, 'eq8tMm6wmJw:APA91bGKz8gm1tS2UtLqYd2OFSgaq1V3O-LrJWcJcKcuSylPjltgIlyLJ7QkerfexY6AJURwJXSTwv-esWVNaD0yJzgw1AqA1ZpGXUHsad-bJM_tfKvQaTAqp8X0jVCtcmkf3R5YTOGA', '2017-09-05 11:57:57', NULL),
(8, 8, 'eq8tMm6wmJw:APA91bGKz8gm1tS2UtLqYd2OFSgaq1V3O-LrJWcJcKcuSylPjltgIlyLJ7QkerfexY6AJURwJXSTwv-esWVNaD0yJzgw1AqA1ZpGXUHsad-bJM_tfKvQaTAqp8X0jVCtcmkf3R5YTOGA', NULL, NULL),
(9, 9, 'eq8tMm6wmJw:APA91bGKz8gm1tS2UtLqYd2OFSgaq1V3O-LrJWcJcKcuSylPjltgIlyLJ7QkerfexY6AJURwJXSTwv-esWVNaD0yJzgw1AqA1ZpGXUHsad-bJM_tfKvQaTAqp8X0jVCtcmkf3R5YTOGA', NULL, NULL),
(10, 10, 'eC_XOOYkb8c:APA91bHNMl3OvrjUNZ74UmNLi7v7cla2js2ft5qlma2DvvdJvcDgl1xQHQkBPtWbce2lVr7Ae_2LpBHsQMANB6Wo6UiQoVumijEfYzv00wEGtanB_QOLk_kuFRcOr4AdCpBbYAsX6dRr', '2017-09-11 07:10:37', NULL),
(11, 11, 'eC_XOOYkb8c:APA91bHNMl3OvrjUNZ74UmNLi7v7cla2js2ft5qlma2DvvdJvcDgl1xQHQkBPtWbce2lVr7Ae_2LpBHsQMANB6Wo6UiQoVumijEfYzv00wEGtanB_QOLk_kuFRcOr4AdCpBbYAsX6dRr', NULL, NULL),
(12, 12, 'eC_XOOYkb8c:APA91bHNMl3OvrjUNZ74UmNLi7v7cla2js2ft5qlma2DvvdJvcDgl1xQHQkBPtWbce2lVr7Ae_2LpBHsQMANB6Wo6UiQoVumijEfYzv00wEGtanB_QOLk_kuFRcOr4AdCpBbYAsX6dRr', NULL, NULL),
(13, 13, 'f6lSUQJYeDw:APA91bFMLQDs2JQKv5a2OSUyqsToBDHIFWGyv5haKfUlRN0LbhHZgU0uRx2u64fHve4uG_IIhBWPj7MMI8w5g2pn5NvBW7pi5RXFaz90HILBgnmP0B4mXueI1-nCV6oAA1ZJGMuBZlMB', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('kamal.baskheti@gmail.com', '$2y$10$JX1XqGc5TzaEucvMer8x1eTl/cPwcsJoBDq0m75/sWr14yNmCrm1a', '2017-06-29 13:50:16');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `service_type` enum('SERVICE','SOCIAL','PUBLIC') COLLATE utf8_unicode_ci NOT NULL,
  `service_sub_type` int(10) UNSIGNED NOT NULL,
  `caption` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `service_nature` enum('FREE','FLAT','HOURLY') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'FREE',
  `service_charge` double(8,2) NOT NULL DEFAULT '0.00',
  `service_duration` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `latitude` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_open` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `service_type`, `service_sub_type`, `caption`, `description`, `service_nature`, `service_charge`, `service_duration`, `latitude`, `longitude`, `is_active`, `is_open`, `created_at`, `updated_at`) VALUES
(1, 1, 'SERVICE', 1, 'Hhh', 'Hhh', 'FREE', 0.00, '0', '43.67094624399068', '-79.38979133963585', 1, 1, '2017-08-21 22:58:31', '2017-08-21 22:58:31'),
(2, 4, 'SERVICE', 2, 'Fdsljkdfsjkl', 'Skjegjklhewrjkerjk', 'FREE', 0.00, '0', '43.67249514209522', '-79.39173091202974', 1, 1, '2017-08-25 14:03:15', '2017-08-25 14:03:15'),
(3, 5, 'SOCIAL', 14, 'Fkfkfkkff', 'Golf', 'FREE', 0.00, '0', '28.62985315370461', '77.36475773155689', 0, 1, '2017-08-25 14:12:49', '2017-08-29 09:12:58'),
(4, 4, 'SERVICE', 3, 'Snan', 'Ndnd', 'FREE', 0.00, '0', '28.62846265699588', '77.39950574934483', 1, 1, '2017-08-25 14:14:46', '2017-08-25 14:14:46'),
(5, 4, 'SERVICE', 1, 'General', 'Ndndjd', 'FREE', 0.00, '0', '28.60862260383039', '77.35735215246677', 1, 1, '2017-08-26 09:45:01', '2017-08-26 09:45:01'),
(6, 5, 'SERVICE', 2, 'Ndndndjdjd', 'Mxmxj', 'FREE', 0.00, '0', '28.60871384998899', '77.35725089907646', 0, 1, '2017-08-28 22:05:41', '2017-08-29 09:13:02'),
(7, 4, 'PUBLIC', 21, 'Etertterterty', 'Retreat', 'FREE', 0.00, '0', '28.60866322309788', '77.35733170062304', 1, 1, '2017-08-28 22:07:21', '2017-08-28 22:07:21'),
(8, 5, 'SERVICE', 1, 'Shan', 'Ndndndjdjd', 'FREE', 0.00, '0', '28.60885690025876', '77.36432790756226', 0, 1, '2017-08-28 22:29:08', '2017-08-29 09:13:07'),
(9, 5, 'SERVICE', 2, 'Ndndndjdjd', 'Ndndndjdjd', 'FREE', 0.00, '0', '28.60876771461961', '77.35735852271318', 0, 1, '2017-08-28 22:31:31', '2017-08-29 09:12:52'),
(10, 5, 'SERVICE', 5, 'Ndndndjdjd', 'Ndndndjdjd', 'FREE', 0.00, '0', '28.60859022614217', '77.3572975024581', 0, 1, '2017-08-28 22:32:24', '2017-08-29 09:12:46'),
(11, 5, 'SERVICE', 2, 'When', 'Ndndndjdjd', 'FREE', 0.00, '0', '28.62906506068872', '77.38977938890457', 1, 1, '2017-08-29 09:13:47', '2017-08-29 09:13:47'),
(12, 5, 'SERVICE', 3, 'Amen', 'Mans', 'FREE', 0.00, '0', '28.6246474514227', '77.38633777946234', 1, 1, '2017-08-29 09:20:37', '2017-08-29 09:20:37'),
(13, 5, 'PUBLIC', 23, 'Ndndndjdjd', 'Ndndndjdjd', 'FREE', 0.00, '0', '28.62423896547164', '77.39549316465855', 1, 1, '2017-08-29 09:32:52', '2017-08-29 09:32:52'),
(14, 5, 'SOCIAL', 19, 'Ndndndjdjd', 'Ndndndjdjd', 'FREE', 0.00, '0', '28.62534758322844', '77.37736210227013', 1, 1, '2017-08-29 09:36:21', '2017-08-29 09:36:21'),
(15, 5, 'SERVICE', 2, 'Ndndndjdjd', 'Ndndndjdjd', 'FREE', 0.00, '0', '28.63427790064611', '77.37819124013186', 1, 1, '2017-08-29 09:46:41', '2017-08-29 09:46:41'),
(16, 5, 'PUBLIC', 20, 'Ndndndjdjd', 'Ndndndjdjd', 'FREE', 0.00, '0', '28.64066955900566', '77.3828549310565', 1, 1, '2017-08-29 09:50:51', '2017-08-29 09:50:51'),
(17, 5, 'SERVICE', 2, 'Dhhdjfjjjjjbb', 'Hdjd', 'FREE', 0.00, '0', '28.63577984735274', '77.38969523459673', 1, 1, '2017-08-29 09:52:19', '2017-08-29 09:52:19'),
(18, 5, 'SERVICE', 6, 'Jdjf', 'Jdjd', 'FREE', 0.00, '0', '28.64073488309266', '77.39656135439873', 1, 1, '2017-08-29 09:53:48', '2017-08-29 09:53:48'),
(19, 5, 'SERVICE', 1, 'Dhhdjfjjjjjbb', 'DJs', 'FREE', 0.00, '0', '28.61729593272495', '77.39085763692856', 1, 1, '2017-08-29 09:57:44', '2017-08-29 09:57:44'),
(20, 5, 'SERVICE', 2, 'Hash', 'Josh\'s', 'FREE', 0.00, '0', '28.62501708863673', '77.38150276243687', 1, 1, '2017-08-29 10:00:59', '2017-08-29 10:00:59'),
(21, 4, 'PUBLIC', 20, 'Ndnd N', 'Ndndndjdjd', 'FREE', 0.00, '0', '28.63028015756825', '77.38681152462959', 1, 1, '2017-08-29 10:53:47', '2017-08-29 10:53:47'),
(25, 6, 'SERVICE', 3, NULL, 'Description', 'FREE', 0.00, '0', '19.0176149944689', '72.8561645373702', 1, 1, '2017-09-03 13:48:47', '2017-09-03 13:48:47'),
(24, 6, 'SERVICE', 1, NULL, 'Description', 'FREE', 0.00, '0', '19.0176149944689', '72.8561645373702', 1, 1, '2017-09-03 13:47:45', '2017-09-03 13:47:45'),
(26, 6, 'SERVICE', 2, NULL, 'Description', 'FREE', 0.00, '0', '19.0176149944689', '72.8561645373702', 1, 1, '2017-09-03 14:57:17', '2017-09-03 14:57:17'),
(27, 7, 'SERVICE', 2, NULL, 'Description', 'FREE', 0.00, '0', '37.78583393502708', '-122.406417131424', 1, 1, '2017-09-05 15:58:03', '2017-09-05 15:58:03'),
(28, 6, 'SERVICE', 2, NULL, 'Description', 'FLAT', 12.00, '12', '37.78583393502708', '-122.406417131424', 1, 1, '2017-09-05 19:53:58', '2017-09-05 19:53:58'),
(29, 9, 'SERVICE', 2, NULL, 'Description', 'FREE', 0.00, '0', '37.78583393502708', '-122.406417131424', 1, 1, '2017-09-05 20:43:57', '2017-09-05 20:43:57'),
(30, 9, 'SERVICE', 1, NULL, 'Description', 'FREE', 0.00, '0', '37.78583393502708', '-122.406417131424', 1, 1, '2017-09-05 21:09:10', '2017-09-05 21:09:10'),
(31, 9, 'SERVICE', 2, NULL, 'Description', 'FLAT', 12.00, '12', '37.78612222281608', '-122.3985421657562', 1, 1, '2017-09-05 21:14:33', '2017-09-05 21:14:33'),
(32, 10, 'SERVICE', 2, NULL, NULL, 'FREE', 0.00, '0', '28.53549985665067', '77.39100012928247', 1, 1, '2017-09-11 11:10:56', '2017-09-11 11:10:56'),
(33, 12, 'SERVICE', 1, NULL, NULL, 'FREE', 0.00, '0', '28.63102145098379', '77.38142564892769', 1, 1, '2017-09-14 13:48:34', '2017-09-14 13:48:34'),
(34, 5, 'SERVICE', 2, NULL, NULL, 'FREE', 0.00, '0', '28.63444357489763', '77.38097637891769', 1, 1, '2017-09-14 13:48:52', '2017-09-14 13:48:52'),
(35, 5, 'SERVICE', 1, NULL, NULL, 'FREE', 0.00, '0', '28.63003090052741', '77.38181456923485', 1, 1, '2017-09-14 14:01:50', '2017-09-14 14:01:50'),
(36, 5, 'SERVICE', 2, NULL, NULL, 'FREE', 0.00, '0', '28.62986139363019', '77.3854623734951', 1, 1, '2017-09-14 14:01:56', '2017-09-14 14:01:56'),
(37, 5, 'SERVICE', 3, NULL, NULL, 'FREE', 0.00, '0', '28.63039257318282', '77.39198818802834', 1, 1, '2017-09-14 14:02:03', '2017-09-14 14:02:03'),
(38, 12, 'SERVICE', 1, NULL, NULL, 'FREE', 0.00, '0', '28.63473166502478', '77.38181188702583', 1, 1, '2017-09-14 14:03:51', '2017-09-14 14:03:51'),
(39, 12, 'SERVICE', 3, NULL, NULL, 'FREE', 0.00, '0', '28.63576748816113', '77.38702610135078', 1, 1, '2017-09-14 14:04:01', '2017-09-14 14:04:01');

-- --------------------------------------------------------

--
-- Table structure for table `post_images`
--

CREATE TABLE `post_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `post_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `privacypolicy`
--

CREATE TABLE `privacypolicy` (
  `id` int(11) UNSIGNED NOT NULL,
  `content` longtext NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `privacypolicy`
--

INSERT INTO `privacypolicy` (`id`, `content`, `created_at`, `updated_at`) VALUES
(1, '<!DOCTYPE html><br /><html><br /><head><br /></head><br /><body>\r\nWelcome to www.lorem-ipsum.info. This site is provided as a service to our visitors and may be used for informational purposes only. Because the Terms and Conditions contain legal obligations, please read them carefully.\r\n\r\n\r\n[b]1. YOUR AGREEMENT[/b]\r\n\r\n\r\nBy using this Site, you agree to be bound by, and to comply with, these Terms and Conditions. If you do not agree to these Terms and Conditions, please do not use this site.\r\n\r\n[quote]PLEASE NOTE: We reserve the right, at our sole discretion, to change, modify or otherwise alter these Terms and Conditions at any time. Unless otherwise indicated, amendments will become effective immediately. Please review these Terms and Conditions periodically. Your continued use of the Site following the posting of changes and/or modifications will constitute your acceptance of the revised Terms and Conditions and the reasonableness of these standards for notice of changes. For your information, this page was last updated as of the date at the top of these terms and conditions.[/quote]  \r\n\r\n\r\n[b]2. PRIVACY[/b]\r\n\r\n\r\nPlease review our Privacy Policy, which also governs your visit to this Site, to understand our practices.\r\n\r\n[b]3. LINKED SITES[/b]\r\n\r\nThis Site may contain links to other independent third-party Web sites (\"Linked Sites&rdquo;). These Linked Sites are provided solely as a convenience to our visitors. Such Linked Sites are not under our control, and we are not responsible for and does not endorse the content of such Linked Sites, including any information or materials contained on such Linked Sites. You will need to make your own independent judgment regarding your interaction with these Linked Sites.\r\n\r\n[b]4. FORWARD LOOKING STATEMENTS[/b]\r\n\r\n\r\nAll materials reproduced on this site speak as of the original date of publication or filing. The fact that a document is available on this site does not mean that the information contained in such document has not been modified or superseded by events or by a subsequent document or filing. We have no duty or policy to update any information or statements contained on this site and, therefore, such information or statements should not be relied upon as being current as of the date you access this site.\r\n\r\n[b]5. DISCLAIMER OF WARRANTIES AND LIMITATION OF LIABILITY[/b]\r\n\r\nA. THIS SITE MAY CONTAIN INACCURACIES AND TYPOGRAPHICAL ERRORS. WE DOES NOT WARRANT THE ACCURACY OR COMPLETENESS OF THE MATERIALS OR THE RELIABILITY OF ANY ADVICE, OPINION, STATEMENT OR OTHER INFORMATION DISPLAYED OR DISTRIBUTED THROUGH THE SITE. YOU EXPRESSLY UNDERSTAND AND AGREE THAT: (i) YOUR USE OF THE SITE, INCLUDING ANY RELIANCE ON ANY SUCH OPINION, ADVICE, STATEMENT, MEMORANDUM, OR INFORMATION CONTAINED HEREIN, SHALL BE AT YOUR SOLE RISK; (ii) THE SITE IS PROVIDED ON AN \"AS IS\" AND \"AS AVAILABLE\" BASIS; (iii) EXCEPT AS EXPRESSLY PROVIDED HEREIN WE DISCLAIM ALL WARRANTIES OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, WORKMANLIKE EFFORT, TITLE AND NON-INFRINGEMENT; (iv) WE MAKE NO WARRANTY WITH RESPECT TO THE RESULTS THAT MAY BE OBTAINED FROM THIS SITE, THE PRODUCTS OR SERVICES ADVERTISED OR OFFERED OR MERCHANTS INVOLVED; (v) ANY MATERIAL DOWNLOADED OR OTHERWISE OBTAINED THROUGH THE USE OF THE SITE IS DONE AT YOUR OWN DISCRETION AND RISK; and (vi) YOU WILL BE SOLELY RESPONSIBLE FOR ANY DAMAGE TO YOUR COMPUTER SYSTEM OR FOR ANY LOSS OF DATA THAT RESULTS FROM THE DOWNLOAD OF ANY SUCH MATERIAL.\r\n\r\n\r\n\r\nB. YOU UNDERSTAND AND AGREE THAT UNDER NO CIRCUMSTANCES, INCLUDING, BUT NOT LIMITED TO, NEGLIGENCE, SHALL WE BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, PUNITIVE OR CONSEQUENTIAL DAMAGES THAT RESULT FROM THE USE OF, OR THE INABILITY TO USE, ANY OF OUR SITES OR MATERIALS OR FUNCTIONS ON ANY SUCH SITE, EVEN IF WE HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. THE FOREGOING LIMITATIONS SHALL APPLY NOTWITHSTANDING ANY FAILURE OF ESSENTIAL PURPOSE OF ANY LIMITED REMEDY.\r\n\r\n\r\n\r\n\r\n \r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<h2>6. EXCLUSIONS AND LIMITATIONS</h2>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\nSOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF CERTAIN WARRANTIES OR THE LIMITATION OR EXCLUSION OF LIABILITY FOR INCIDENTAL OR CONSEQUENTIAL DAMAGES. ACCORDINGLY, OUR LIABILITY IN SUCH JURISDICTION SHALL BE LIMITED TO THE MAXIMUM EXTENT PERMITTED BY LAW.\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<h2>7. OUR PROPRIETARY RIGHTS</h2>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\nThis Site and all its Contents are intended solely for personal, non-commercial use. Except as expressly provided, nothing within the Site shall be construed as conferring any license under our or any third party\'s intellectual property rights, whether by estoppel, implication, waiver, or otherwise. Without limiting the generality of the foregoing, you acknowledge and agree that all content available through and used to operate the Site and its services is protected by copyright, trademark, patent, or other proprietary rights. You agree not to: (a) modify, alter, or deface any of the trademarks, service marks, trade dress (collectively \"Trademarks\") or other intellectual property made available by us in connection with the Site; (b) hold yourself out as in any way sponsored by, affiliated with, or endorsed by us, or any of our affiliates or service providers; (c) use any of the Trademarks or other content accessible through the Site for any purpose other than the purpose for which we have made it available to you; (d) defame or disparage us, our Trademarks, or any aspect of the Site; and (e) adapt, translate, modify, decompile, disassemble, or reverse engineer the Site or any software or programs used in connection with it or its products and services.\r\n\r\nThe framing, mirroring, scraping or data mining of the Site or any of its content in any form and by any method is expressly prohibited.\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<h2>8. INDEMNITY</h2>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\nBy using the Site web sites you agree to indemnify us and affiliated entities (collectively \"Indemnities\") and hold them harmless from any and all claims and expenses, including (without limitation) attorney\'s fees, arising from your use of the Site web sites, your use of the Products and Services, or your submission of ideas and/or related materials to us or from any person\'s use of any ID, membership or password you maintain with any portion of the Site, regardless of whether such use is authorized by you.\r\n</body><br /></html>', '2017-07-24 18:30:00', '2017-08-18 17:58:40');

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE `profile` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `bio` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dob` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `education` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`id`, `user_id`, `bio`, `dob`, `education`, `address`, `latitude`, `longitude`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL, NULL, 'Yorkville Toronto Canada', '43.6709201536075', '-79.38980644612077', NULL, '2017-08-21 22:33:47'),
(2, 2, 'Intelligent', '31/08/1994', 'MTech', 'Sector 63 Gautambudh Nagar India', '28.63006802627025', '77.38181553469208', NULL, '2017-08-23 14:00:00'),
(3, 3, NULL, NULL, NULL, 'Sector 63 Gautambudh Nagar India', '28.63005181175146', '77.38182037565052', NULL, NULL),
(4, 4, 'test', '08/25/2017', 'test', 'Yorkville Toronto Canada', '43.67249505430848', '-79.391731', NULL, '2017-08-25 14:47:14'),
(5, 5, NULL, '24 December, 1991', NULL, 'Sector 82 Gautambudh Nagar India', '28.5355', '77.39100000000001', NULL, '2017-09-14 12:31:03'),
(6, 6, 'Jkfhadfjkadsfhasdkjfhsdkjfhasdfjksadhfklsadhfsdjfhasdjlkfhsdfjasdlfhskajlfhsdkjfasdfhsadfjkasdkjfhsadkjfhasdkjfhasdkjfhsajkfhsadfksadhfjkasdfhajskdfhjasdkfhasdjkfhasdjkfhasdjkfhasdjkfhadsjkfhaksdjfhasjkldfhasdjklfhasjkdfhasldjkfh', '5 September, 1999', 'Sdfaskjfhalsjkdfhalkjfhkjsalf', 'Union Square San Francisco United States', '37.785834', '-122.406417', NULL, '2017-09-05 19:43:01'),
(7, 7, NULL, NULL, NULL, 'Matunga East Mumbai India', '19.017615', '72.8561644', NULL, NULL),
(8, 8, NULL, NULL, NULL, 'Union Square San Francisco United States', '37.785834', '-122.406417', NULL, NULL),
(9, 9, NULL, NULL, NULL, 'Union Square San Francisco United States', '37.785834', '-122.406417', NULL, '2017-09-05 20:20:48'),
(10, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 11, NULL, NULL, NULL, 'Sector 82 Gautambudh Nagar India', '28.5355', '77.391', NULL, NULL),
(12, 12, NULL, NULL, NULL, 'Sector 82 Gautambudh Nagar India', '28.5355', '77.391', NULL, NULL),
(13, 13, NULL, NULL, NULL, 'Sector 63 Gautambudh Nagar India', '28.63010711804477', '77.38185570036701', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `receiver_id` int(10) UNSIGNED NOT NULL,
  `sender_id` int(10) UNSIGNED NOT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `rating` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `receiver_id`, `sender_id`, `comment`, `rating`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 5, 4, 'Try', 3, 1, NULL, '2017-08-29 09:04:00');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `notification` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `user_id`, `notification`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 2, 1, NULL, NULL),
(3, 3, 1, NULL, NULL),
(4, 4, 1, NULL, NULL),
(5, 5, 1, NULL, NULL),
(6, 6, 1, NULL, NULL),
(7, 7, 1, NULL, NULL),
(8, 8, 1, NULL, NULL),
(9, 9, 1, NULL, NULL),
(10, 10, 1, NULL, NULL),
(11, 11, 1, NULL, NULL),
(12, 12, 1, NULL, NULL),
(13, 13, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

CREATE TABLE `skills` (
  `id` int(10) UNSIGNED NOT NULL,
  `skill` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `uniqueID` int(4) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `skills`
--

INSERT INTO `skills` (`id`, `skill`, `uniqueID`, `is_active`) VALUES
(1, 'NONE', 0, 1),
(2, 'GENERAL', 1, 1),
(3, 'PAINTING', 2, 1),
(4, 'SNOW REMOVAL', 3, 1),
(5, 'LANDSCAPING', 4, 1),
(6, 'DOG WALKING', 5, 1),
(7, 'BABYSITTING', 6, 1),
(8, 'VOLUNTEERING', 7, 1),
(9, 'TUTOR', 8, 1),
(10, 'HEAVY LABOUR', 9, 1),
(11, 'VEHICLE NEEDED', 10, 1),
(12, 'HOUSE CHORES', 11, 1),
(13, 'CLEANING', 12, 1),
(14, 'PICKUP SPORTS', 13, 1),
(15, 'STREET PARTY', 14, 1),
(16, 'START A CLUB', 15, 1),
(17, 'RUNNING', 16, 1),
(18, 'CYCLING', 17, 1),
(19, 'JAM OUT', 18, 1),
(20, 'FUND RAISING', 19, 1),
(21, 'MY RESTAURANT', 20, 1),
(22, 'MY BUSINESS', 21, 1),
(23, 'GARAGE SALE', 22, 1),
(24, 'LIVE MUSIC', 23, 1),
(25, 'SPORTING EVENT', 24, 1);

-- --------------------------------------------------------

--
-- Table structure for table `termsandcondition`
--

CREATE TABLE `termsandcondition` (
  `id` int(11) UNSIGNED NOT NULL,
  `content` longtext NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `termsandcondition`
--

INSERT INTO `termsandcondition` (`id`, `content`, `created_at`, `updated_at`) VALUES
(1, '<!DOCTYPE html><br /><html><br /><head><br /></head><br /><body>\r\nWelcome to www.lorem-ipsum.info. This site is provided as a service to our visitors and may be used for informational purposes only. Because the Terms and Conditions contain legal obligations, please read them carefully.\r\n\r\n\r\n\r\n<h2>1. YOUR AGREEMENT</h2>\r\n\r\nBy using this Site, you agree to be bound by, and to comply with, these Terms and Conditions. If you do not agree to these Terms and Conditions, please do not use this site.\r\n\r\n[quote]PLEASE NOTE: We reserve the right, at our sole discretion, to change, modify or otherwise alter these Terms and Conditions at any time. Unless otherwise indicated, amendments will become effective immediately. Please review these Terms and Conditions periodically. Your continued use of the Site following the posting of changes and/or modifications will constitute your acceptance of the revised Terms and Conditions and the reasonableness of these standards for notice of changes. For your information, this page was last updated as of the date at the top of these terms and conditions.[/quote]  \r\n\r\n<h2>2. PRIVACY</h2>\r\nPlease review our Privacy Policy, which also governs your visit to this Site, to understand our practices.\r\n\r\n\r\n\r\n\r\n<h2>3. LINKED SITES</h2>\r\n\r\nThis Site may contain links to other independent third-party Web sites (\"Linked Sites&rdquo;). These Linked Sites are provided solely as a convenience to our visitors. Such Linked Sites are not under our control, and we are not responsible for and does not endorse the content of such Linked Sites, including any information or materials contained on such Linked Sites. You will need to make your own independent judgment regarding your interaction with these Linked Sites.\r\n\r\n\r\n\r\n\r\n<h2>4. FORWARD LOOKING STATEMENTS</h2>\r\nAll materials reproduced on this site speak as of the original date of publication or filing. The fact that a document is available on this site does not mean that the information contained in such document has not been modified or superseded by events or by a subsequent document or filing. We have no duty or policy to update any information or statements contained on this site and, therefore, such information or statements should not be relied upon as being current as of the date you access this site.\r\n\r\n\r\n\r\n\r\n\r\n<h2>5. DISCLAIMER OF WARRANTIES AND LIMITATION OF LIABILITY</h2>\r\n\r\n\r\n\r\n\r\nA. THIS SITE MAY CONTAIN INACCURACIES AND TYPOGRAPHICAL ERRORS. WE DOES NOT WARRANT THE ACCURACY OR COMPLETENESS OF THE MATERIALS OR THE RELIABILITY OF ANY ADVICE, OPINION, STATEMENT OR OTHER INFORMATION DISPLAYED OR DISTRIBUTED THROUGH THE SITE. YOU EXPRESSLY UNDERSTAND AND AGREE THAT: (i) YOUR USE OF THE SITE, INCLUDING ANY RELIANCE ON ANY SUCH OPINION, ADVICE, STATEMENT, MEMORANDUM, OR INFORMATION CONTAINED HEREIN, SHALL BE AT YOUR SOLE RISK; (ii) THE SITE IS PROVIDED ON AN \"AS IS\" AND \"AS AVAILABLE\" BASIS; (iii) EXCEPT AS EXPRESSLY PROVIDED HEREIN WE DISCLAIM ALL WARRANTIES OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, WORKMANLIKE EFFORT, TITLE AND NON-INFRINGEMENT; (iv) WE MAKE NO WARRANTY WITH RESPECT TO THE RESULTS THAT MAY BE OBTAINED FROM THIS SITE, THE PRODUCTS OR SERVICES ADVERTISED OR OFFERED OR MERCHANTS INVOLVED; (v) ANY MATERIAL DOWNLOADED OR OTHERWISE OBTAINED THROUGH THE USE OF THE SITE IS DONE AT YOUR OWN DISCRETION AND RISK; and (vi) YOU WILL BE SOLELY RESPONSIBLE FOR ANY DAMAGE TO YOUR COMPUTER SYSTEM OR FOR ANY LOSS OF DATA THAT RESULTS FROM THE DOWNLOAD OF ANY SUCH MATERIAL.\r\n\r\n\r\nB. YOU UNDERSTAND AND AGREE THAT UNDER NO CIRCUMSTANCES, INCLUDING, BUT NOT LIMITED TO, NEGLIGENCE, SHALL WE BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, PUNITIVE OR CONSEQUENTIAL DAMAGES THAT RESULT FROM THE USE OF, OR THE INABILITY TO USE, ANY OF OUR SITES OR MATERIALS OR FUNCTIONS ON ANY SUCH SITE, EVEN IF WE HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. THE FOREGOING LIMITATIONS SHALL APPLY NOTWITHSTANDING ANY FAILURE OF ESSENTIAL PURPOSE OF ANY LIMITED REMEDY.\r\n\r\n\r\n\r\n\r\n\r\n<h2>6. EXCLUSIONS AND LIMITATIONS</h2>\r\n\r\n\r\n\r\n\r\nSOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF CERTAIN WARRANTIES OR THE LIMITATION OR EXCLUSION OF LIABILITY FOR INCIDENTAL OR CONSEQUENTIAL DAMAGES. ACCORDINGLY, OUR LIABILITY IN SUCH JURISDICTION SHALL BE LIMITED TO THE MAXIMUM EXTENT PERMITTED BY LAW.\r\n\r\n\r\n\r\n\r\n<h2>7. OUR PROPRIETARY RIGHTS</h2>\r\n\r\n\r\n\r\n\r\nThis Site and all its Contents are intended solely for personal, non-commercial use. Except as expressly provided, nothing within the Site shall be construed as conferring any license under our or any third party\'s intellectual property rights, whether by estoppel, implication, waiver, or otherwise. Without limiting the generality of the foregoing, you acknowledge and agree that all content available through and used to operate the Site and its services is protected by copyright, trademark, patent, or other proprietary rights. You agree not to: (a) modify, alter, or deface any of the trademarks, service marks, trade dress (collectively \"Trademarks\") or other intellectual property made available by us in connection with the Site; (b) hold yourself out as in any way sponsored by, affiliated with, or endorsed by us, or any of our affiliates or service providers; (c) use any of the Trademarks or other content accessible through the Site for any purpose other than the purpose for which we have made it available to you; (d) defame or disparage us, our Trademarks, or any aspect of the Site; and (e) adapt, translate, modify, decompile, disassemble, or reverse engineer the Site or any software or programs used in connection with it or its products and services.\r\n\r\nThe framing, mirroring, scraping or data mining of the Site or any of its content in any form and by any method is expressly prohibited.\r\n\r\n\r\n\r\n<h2>8. INDEMNITY</h2>\r\n\r\nBy using the Site web sites you agree to indemnify us and affiliated entities (collectively \"Indemnities\") and hold them harmless from any and all claims and expenses, including (without limitation) attorney\'s fees, arising from your use of the Site web sites, your use of the Products and Services, or your submission of ideas and/or related materials to us or from any person\'s use of any ID, membership or password you maintain with any portion of the Site, regardless of whether such use is authorized by you.\r\n</body><br /></html>', '2017-06-02 17:35:24', '2017-08-18 19:56:29');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `api_token` text COLLATE utf8_unicode_ci,
  `profile_image` text COLLATE utf8_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_skipped` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `facebook_token`, `api_token`, `profile_image`, `remember_token`, `is_skipped`, `created_at`, `updated_at`) VALUES
(1, 'ABDALLAH HAJI', 'abdallahhaji@gmail.com', '$2y$10$kXJLAmkSTuBWIoHVoC16E.s18jmMhm7RwRVTNEfPg8j3xW5GM.miK', '10155085882030552', NULL, 'http://sparehandz.apphosthub.com/public/images/uploads/profile_599b278b2ed3c.png', NULL, 0, '2017-08-21 22:33:38', '2017-08-21 22:33:47'),
(2, 'MIKE', 'mike@gmail.com', '$2y$10$0tkTJfm2.4okflKkUTPC2eOdaiDPpJ978mbrb1SqzIGA67tGzvXBu', NULL, NULL, 'http://sparehandz.apphosthub.com/public/images/uploads/profile_599d522003cab.png', NULL, 0, '2017-08-23 13:58:54', '2017-08-23 14:00:00'),
(3, 'ALOK', 'alokmodi37@gmail.com', '$2y$10$tkWuQ03eCQ6ISxGrH97n6u4plmudU87uQfxmKJkncWLb1u0Bmk/Lq', NULL, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjMsImlzcyI6Imh0dHA6Ly9zcGFyZWhhbmR6LmFwcGhvc3RodWIuY29tL2FwaS92MS9hdXRoL2xvZ2luIiwiaWF0IjoxNTAzNTY2MzM1LCJleHAiOjE1MTM5MzQzMzUsIm5iZiI6MTUwMzU2NjMzNSwianRpIjoiS0VnNEI3Z2Z1N2Q3NDN1TiJ9.BQAlHmoGABju11fnd43RTLKf-On8diU1Go_mcAZnEy0', '', NULL, 0, '2017-08-24 13:17:32', '2017-08-24 13:18:55'),
(4, 'ABC', 'a@a.com', '$2y$10$kMuxkJN2jM0D.FKeFE1ROOWC05JwPo5ruVuSo/2Ix1C4PGaAkEkda', NULL, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjQsImlzcyI6Imh0dHA6Ly9zcGFyZWhhbmR6LmFwcGhvc3RodWIuY29tL2FwaS92MS9hdXRoL2xvZ2luIiwiaWF0IjoxNTAzOTg5NjY5LCJleHAiOjE1MTQzNTc2NjksIm5iZiI6MTUwMzk4OTY2OSwianRpIjoicGVVTFFSUmY5eWtGcjVhSSJ9.AkzMa7vVOb8k5uVd89zvdn_qTbo88pBdrDL33vLzaw0', 'http://sparehandz.apphosthub.com/public/images/uploads/profile_59a00032643ac.png', NULL, 0, '2017-08-25 14:01:54', '2017-08-29 10:54:29'),
(5, 'LEO', 'leo@gmail.com', '$2y$10$04aYnuqjG70tDYRiwZwDhO4ckf5A0rgsSm/uGa2KD.BQWjITin6zO', NULL, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjUsImlzcyI6Imh0dHA6Ly9zcGFyZWhhbmR6LmFwcGhvc3RodWIuY29tL2FwaS92MS9hdXRoL2xvZ2luIiwiaWF0IjoxNTA1Mzg0NzUxLCJleHAiOjE1MTU3NTI3NTEsIm5iZiI6MTUwNTM4NDc1MSwianRpIjoiUHZWbGY0QnVBUldtcjZTayJ9.isTw8_BSJYy5iQwuuoPNahuMPZfd6Alxt-YVM719gsg', 'http://sparehandz.apphosthub.com/public/images/uploads/profile_59ba3e4746b74.png', NULL, 0, '2017-08-25 14:03:49', '2017-09-14 14:25:51'),
(6, 'LEO', 'leoo@gmail.com', '$2y$10$/6yTibgr638qrRm.QviUsO0TAUu23qy6If9qIHb202pFDs06ASXsq', NULL, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjYsImlzcyI6Imh0dHA6Ly9zcGFyZWhhbmR6LmFwcGhvc3RodWIuY29tL2FwaS92MS9hdXRoL2xvZ2luIiwiaWF0IjoxNTA0NjI4MTczLCJleHAiOjE1MTQ5OTYxNzMsIm5iZiI6MTUwNDYyODE3MywianRpIjoibm1ndnJXMndvcXF1blBpWCJ9.e-0GkWqfx97M8a70wqaTsEi12zWrfErWG5zdeXgeoIU', 'http://sparehandz.apphosthub.com/public/images/uploads/profile_59aec605c0234.png', NULL, 0, '2017-09-03 13:23:01', '2017-09-05 20:16:13'),
(7, 'HEX', 'hexo@gmail.com', '$2y$10$tPNBSNblB6pRD6.uf4F7F.9GSRTqKRiyx97b9e71ls5tZeILEC7ha', NULL, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjcsImlzcyI6Imh0dHA6Ly9zcGFyZWhhbmR6LmFwcGhvc3RodWIuY29tL2FwaS92MS9hdXRoL2xvZ2luIiwiaWF0IjoxNTA0NjEyNjc2LCJleHAiOjE1MTQ5ODA2NzYsIm5iZiI6MTUwNDYxMjY3NiwianRpIjoiQXp1QVluRWNDY0lRM0NiWiJ9.oY2VqXO_p8jhIHwp4NJ9xNs7KYuqH7Swh7PpQ2b4jHU', '', NULL, 0, '2017-09-03 14:57:53', '2017-09-05 15:57:56'),
(8, 'JOHN SMITH', 'apptesting2000@gmail.com', '$2y$10$.6es8WsC2ipBXbrUTUdXBO2A3hPokZLrnaWC8jVXHliKJLHUH.Nm2', '306734413114037', NULL, '', NULL, 0, '2017-09-05 20:16:50', '2017-09-05 20:16:50'),
(9, 'ADDS', 'q@q.com', '$2y$10$C9Sw/sGhGFp5YwekVnUzXuzXTz2y6nDyNS6oqrIkrvS4M8QCq55Ai', NULL, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjksImlzcyI6Imh0dHA6Ly9zcGFyZWhhbmR6LmFwcGhvc3RodWIuY29tL2FwaS92MS9hdXRoL2xvZ2luIiwiaWF0IjoxNTA0NjI4NDE3LCJleHAiOjE1MTQ5OTY0MTcsIm5iZiI6MTUwNDYyODQxNywianRpIjoieVdXZVRYWDFKODlteXJNYyJ9.VQ7_erH3FyEQuwXk9euaBycxKeGnQpsYrQ4ZyRUDTzQ', 'http://sparehandz.apphosthub.com/public/images/uploads/profile_59aecee0ed394.png', NULL, 0, '2017-09-05 20:19:27', '2017-09-05 21:09:41'),
(10, 'ROGER', 't@t.com', '$2y$10$p10pkKmSLaf87P0/wu.eoehJim1X6UilYIP/ctCUolbJ4P7jWNese', NULL, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwLCJpc3MiOiJodHRwOi8vc3BhcmVoYW5kei5hcHBob3N0aHViLmNvbS9hcGkvdjEvYXV0aC9sb2dpbiIsImlhdCI6MTUwNTExNzA5NCwiZXhwIjoxNTE1NDg1MDk0LCJuYmYiOjE1MDUxMTcwOTQsImp0aSI6IkNnQjJLTHNZWWNXT1d5Qm4ifQ.SJJni7Ptz8a12EwYqDMhCkdUUoF8jDnftsLyvqaxwdU', '', NULL, 0, '2017-09-11 10:56:23', '2017-09-11 12:04:54'),
(11, 'MILAN', 'm@m.com', '$2y$10$OTaZqcla3icr3SEIW7tVwebPg7v95QHlk09G0S5nN9QeqbL/8h1Ue', NULL, NULL, '', NULL, 0, '2017-09-11 11:11:20', '2017-09-11 11:11:20'),
(12, 'EGO', 'geo@gmail.com', '$2y$10$NsdCuvAXp6C1acotV2/pUeT1.lFm9j3OIVXcFe6.7BQl2Wic//cwC', NULL, NULL, '', NULL, 0, '2017-09-14 12:31:22', '2017-09-14 12:31:22'),
(13, 'RISHABH JAIN', 'rishabh@brainmobi.com', '$2y$10$6QpWnJfqwmUFrL/IvSItJ.jw8gcdEOa9hh9Pv89bznAOBDLtmTeyC', '1154806927998671', NULL, 'https://scontent.xx.fbcdn.net/v/t1.0-1/p200x200/20882381_1232664316879598_3655019658723472124_n.jpg?oh=d9e6f4ff42510e56a552b9d31d494da8&oe=5A14BC42', NULL, 0, '2017-09-14 14:44:33', '2017-09-14 14:44:33');

-- --------------------------------------------------------

--
-- Table structure for table `user_skills`
--

CREATE TABLE `user_skills` (
  `id` int(10) UNSIGNED NOT NULL,
  `skill_id` int(10) DEFAULT NULL,
  `profile_id` int(10) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_skills`
--

INSERT INTO `user_skills` (`id`, `skill_id`, `profile_id`, `is_active`) VALUES
(1, 3, 2, 1),
(2, 4, 2, 1),
(3, 5, 2, 1),
(4, 2, 4, 1),
(20, 5, 6, 1),
(19, 4, 6, 1),
(18, 3, 6, 1),
(17, 2, 6, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `applicants`
--
ALTER TABLE `applicants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `applicants_user_id_foreign` (`user_id`),
  ADD KEY `applicants_post_id_foreign` (`post_id`),
  ADD KEY `index_applicant_listing` (`user_id`,`post_id`);

--
-- Indexes for table `chat_head`
--
ALTER TABLE `chat_head`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `connections`
--
ALTER TABLE `connections`
  ADD PRIMARY KEY (`id`),
  ADD KEY `connections_user_id_foreign` (`user_id`),
  ADD KEY `connections_post_id_foreign` (`post_id`),
  ADD KEY `index_connections_listing` (`user_id`,`post_user_id`,`post_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification_token`
--
ALTER TABLE `notification_token`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_user_id_foreign` (`user_id`),
  ADD KEY `posts_service_sub_type_foreign` (`service_sub_type`),
  ADD KEY `index_posts_listing` (`user_id`,`latitude`,`longitude`);

--
-- Indexes for table `post_images`
--
ALTER TABLE `post_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_images_post_id_foreign` (`post_id`);

--
-- Indexes for table `privacypolicy`
--
ALTER TABLE `privacypolicy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profile_user_id_foreign` (`user_id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reviews_receiver_id_foreign` (`receiver_id`),
  ADD KEY `reviews_sender_id_foreign` (`sender_id`),
  ADD KEY `index_review_listing` (`sender_id`,`receiver_id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`),
  ADD KEY `setting_user_id_foreign` (`user_id`);

--
-- Indexes for table `skills`
--
ALTER TABLE `skills`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `termsandcondition`
--
ALTER TABLE `termsandcondition`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_skills`
--
ALTER TABLE `user_skills`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `applicants`
--
ALTER TABLE `applicants`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `chat_head`
--
ALTER TABLE `chat_head`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `connections`
--
ALTER TABLE `connections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `notification_token`
--
ALTER TABLE `notification_token`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `post_images`
--
ALTER TABLE `post_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `privacypolicy`
--
ALTER TABLE `privacypolicy`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `profile`
--
ALTER TABLE `profile`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `skills`
--
ALTER TABLE `skills`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `termsandcondition`
--
ALTER TABLE `termsandcondition`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `user_skills`
--
ALTER TABLE `user_skills`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
