<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Skill extends Authenticatable
{
    use Notifiable;

    protected $table = 'skills';
    public $timestamps = false;
    protected $hidden = ['is_active'];
}