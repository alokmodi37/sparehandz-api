<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Service extends Authenticatable
{
    use Notifiable;

    protected $table = 'services';
    public $timestamps = false;
     protected $hidden = ['is_active'];
}
