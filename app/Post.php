<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Post extends Authenticatable
{
    use Notifiable;

    protected $table = 'posts';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   
    protected $fillable = [
        'user_id', 'service_type', 'service_sub_type', 'caption', 'description', 'service_nature', 'service_charge', 'service_duration'
    ];

    protected $hidden = ['is_active', 'updated_at', 'created_at', 'user_id', 'service_sub_type'];

    public function postImages()
    {
        return $this->hasMany('App\PostImage');
    }
}