<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Review extends Authenticatable
{
    use Notifiable;

    protected $table = 'reviews';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    protected $fillable = [
        'receiver_id', 'sender_id', 'comment', 'rating'
    ];

    protected $hidden = ['created_at', 'updated_at', 'is_active'];
}