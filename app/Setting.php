<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Setting extends Authenticatable
{
    use Notifiable;

    protected $table = 'setting';
    protected $hidden = ['id', 'user_id', 'created_at', 'updated_at'];
}
