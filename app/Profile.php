<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Profile extends Authenticatable
{
    use Notifiable;

    protected $table = 'profile';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   
    protected $fillable = [
        'user_id', 'skill_id', 'bio', 'dob', 'education', 'address', 'latitude', 'longitude'
    ];

    protected $hidden = ['user_id', 'skill_id', 'is_active', 'created_at', 'updated_at'];
}
