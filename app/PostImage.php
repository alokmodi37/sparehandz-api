<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class PostImage extends Authenticatable
{
    use Notifiable;

    protected $table = 'post_images';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $timestamps = false;
    protected $fillable = [
        'post_id', 'post_image'
    ];

    protected $hiden = ['id', 'post_id', 'is_active'];

    public function profile()
    {
        return $this->belongsTo('App\Post');
    }
}