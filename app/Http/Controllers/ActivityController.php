<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use JWTAuthException;
use App\User;
use App\Post as Post;
use App\Connection as Connection;
use App\Applicant as Applicant;

class ActivityController extends Controller
{
	public function __construct(Request $request){
		$this->user = JWTAuth::toUser($request->token);
	}

    public function applied(Request $request){
    	
    	return response()->json([
    		'error_code' => 200, 
    		'msg_string' => 'User Applied for', 
    		'result' => Applicant::where('user_id', $this->user->id)->get()
    	]);
    }

    public function posts(Request $request){
    	
    	return response()->json([
    		'error_code' => 200, 
    		'msg_string' => 'User Posts', 
    		'result' => Post::where('user_id', $this->user->id)->get()
    	]);
    }

    public function pending(Request $request){
    	
    	return response()->json([
    		'error_code' => 200, 
    		'msg_string' => 'User Pending request', 
    		'result' => Post::where('user_id', $this->user->id)->select('id')->get()
    	]);
    }

    public function connection(Request $request){
    	
    	return response()->json([
    		'error_code' => 200, 
    		'msg_string' => 'User Connections', 
    		'result' => Connection::where('user_id', $this->user->id)->get()
    	]);
    }


    public function approveConnection(Request $request){}

    public function rejectConnection(Request $request){}
}
