<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use JWTAuthException;

use App\Review as Review;

class RatingController extends Controller
{
	public function getCurrentUser($request) {
		return JWTAuth::toUser($request->token);
	}

    public function saveRating(Request $request) {
    	$ratingData = $request->only('receiver_id', 'comment', 'rating');
    	$ratingData['sender_id'] = $this->getCurrentUser($request)->id;

    	$ratingQuery = Review::where('receiver_id', $ratingData['receiver_id'])
    						->where('sender_id', $ratingData['sender_id']);

    	$ratingRow = $ratingQuery->get();
		if( count($ratingRow->toArray()) ) {

			unset($ratingData['sender_id']);
			unset($ratingData['receiver_id']);

			$ratingQuery->update($ratingData);

			return response()->json(['result' => 'Updated successfully']);
		} else {	
    		Review::insert($ratingData);
    		return response()->json(['result' => 'Profile rated successfully']);
    	}
    }

    public function getRating(Request $request) {
    	if( !($user = $request->input('id', null)) )
    		$user = $this->getCurrentUser($request)->id;
    	
      	$userRating =  Review::where('receiver_id', $user)->get();
      
    	return response()->json(['result' => $userRating]);
    }
}
