<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User as User;

class CommonController extends Controller
{
    public function resetPassword(Request $request, $token=null){
    	$userRow = User::where('remember_token', $token)->count();

    	if($userRow) {
    		$validation = Validator::make($request->all(), [ 
		            'password' => 'required|min:8',
		            'repassword' => 'required|min:8|same:password'
		    ]);

	    	$errors = $validation->errors();

	    	if($request->all()) {

	    		if($validation->fails()){
	    			return view('app.reset-password', ['token' => $token, 'message' => '', 'errors' => $errors]);
	    		}

	    		$userRow = User::where('remember_token', $token)->first();
	    		$resetData = $request->only('password', 'repassword');	

	    		if($resetData['password'] && ($resetData['password'] == $resetData['repassword'])) {
	    			$userRow->update(
		    			[
		    				'password' => bcrypt($resetData['password']),
		    				'remember_token' => ''
		    			]
		    		);

		    		return view('app.success-message', ['message' => 'Password has been rest, Please login with your new credentials']);
	    		} else {
	    			 return view('app.reset-password', ['token' => $token, 'message' => '', 'errors' => $errors]);
	    		}
	    		
	    	} else {
	  	        return view('app.reset-password', ['token' => $token, 'message' => '', 'errors' => $errors]);
	    	}
	    } else {
	    	return view('app.error-message', ['message' => 'Invalid token provided']);
		}    	 
    }

}
