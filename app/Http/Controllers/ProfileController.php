<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use JWTAuthException;
use App\User;
use App\Profile as Profile;
use App\Skill as Skill;
use App\Setting as Setting;

class ProfileController extends Controller
{
	public function getCurrentUser($request) {
		return JWTAuth::toUser($request->token);
	}

    public function getSkills(Request $request){
        return response()->json(['result' => Skill::all()]);
    }

    public function saveSetting(Request $request){
       $settingData = $request->only('notification');
       $user = $this->getCurrentUser($request);

       $settingQuery = Setting::where('user_id', $user->id);

       $settingRow =  $settingQuery->get();

      
       if( count($settingRow->toArray()) ) {
            foreach($settingData as $field => $value){
                if(!$value) {
                    unset($settingData[$field]);
                }
            }

            $settingQuery->update($settingData);
            return response()->json(['result' => 'Setting updated successfully']);
        } else {
            $settingData['user_id'] = $user->id;
            
            Setting::insert($settingData);
            return response()->json(['result' => 'Setting created successfully']);
        }       
    }

    public function getSetting(Request $request){
        $user = $this->getCurrentUser($request)->id;

        $settings = Setting::where('user_id', $user)->get();
        return response()->json(['result' => $settings]);
    }

    public function getProfile(Request $request){
    	if( !($user = $request->input('id', null)) )
    		$user = $this->getCurrentUser($request)->id;

    	$userProfile = Profile::join('skills', 'skills.id', '=', 'profile.skill_id')
    						->join('users', 'users.id', '=', 'profile.user_id')
    						->where('profile.user_id', $user)
    						->select('profile.*', 'users.name', 'users.email', 'skills.skill')
    						->get();

    	$userProfile = $userProfile->toArray();

    	if($userProfile) {
    		$userProfile[0]['service_images'] = Profile::find($userProfile[0]['id'])->serviceImages;
    		return response()->json(['result' => $userProfile]);
    	} else {
    		return response()->json(['exception' => 'profile not found']);
    	}
    }

    public function saveProfile(Request $request){
    	$user = $this->getCurrentUser($request);

    	$userQuery = Profile::where('profile.user_id', $user->id);

    	$userProfile = 	$userQuery->get();
		
		$profileData = $request->only('profile_image', 'about', 'dob', 'address', 'latitude', 'longitude');

    	if( count($userProfile->toArray()) ) {
    		foreach($profileData as $field => $value){
    			if(!$value) {
    				unset($profileData[$field]);
    			}
    		}

    		$userQuery->update($profileData);
    		return response()->json(['result' => 'Profile updated successfully']);
    	} else {
    		$profileData['user_id'] = $user->id;
    		
	   		Profile::insert($profileData);
	   		return response()->json(['result' => 'Profile created successfully']);
    	}					
    	
    } 	
}
