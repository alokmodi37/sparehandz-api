<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use App\User as User;
use App\Profile as Profile;
use JWTAuthException;

// Email notification section
use App\Notifications\RegistrationSuccess as RegistrationSuccess;
use App\Notifications\ForgotPasswordNotification as ForgotPasswordNotification;

class UserController extends Controller
{   
    private $user;
    public function __construct(User $user){
        $this->user = $user;
    }
   
    public function register(Request $request){
       

        $validation = Validator::make($request->all(), [ 
            'name' => 'required'
        ]);

        if($validation->fails()){
           $errors = $validation->errors();

           return response()->json(['error_code'=>500,'msg_string'=>'Missing required provide', 'result' => $errors]);

        } else {

              $isUserRegistered = User::where('email', $request->get('email', null))->count();
              $isFacebookUserRegistered = User::where('facebook_token', $request->get('facebook_token', 0))->count();

              if ($isFacebookUserRegistered) {
                return $this->login($request);
              }

              if( $isUserRegistered) {
                  if($facebookToken = $request->get('facebook_token', 0)) {
                    $user = User::where('email', $request->get('email', null))->first();
                    $user->update(['facebook_token' => $facebookToken]);
                    
                    return response()->json(['error_code'=>200,'msg_string'=>'Token created successful','token'=>JWTAuth::fromUser($user)]);
                  }

                  return response()->json(['error_code'=>500, 'msg_string'=>'Already registered. Try another','token'=>[]]);
              }

              $password = $request->get('password', $request->get('email')."secret");
       
              $user = $this->user->create([
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'password' => bcrypt($password),
                'profile_image'=>$request->get('profile_image', null),
                'facebook_token' => $request->get('facebook_token', null)
              ]);

              $user->notify(new RegistrationSuccess());
       
              // Create a profile for the user;
              $hasProfile = Profile::where('profile.user_id', $user->id)->count();

              if (!$hasProfile) {
                  $profileData = $request->only('bio', 'education', 'dob', 'address', 'latitude', 'longitude');
                  $profileData['user_id'] = $user->id;
                  $profileData['skill_id'] = 1;

                  Profile::insert($profileData);
              }

              // JWTAuth::attempt($credentials)

              // $credentials = ['email' => $request->get('email'), 'password' => $password];
              return response()->json(['error_code'=>200,'msg_string'=>'User created successfully','token'=>JWTAuth::fromUser($user)]);
        }
    }

    public function login(Request $request){

       if($request->get('facebook_token')){
          $isUserExist = User::where('facebook_token', $request->get('facebook_token'))->count();
          
          
          if($isUserExist) {
              $currentUser = User::where('facebook_token', $request->get('facebook_token'))->first();
             
              try {
                 $token = JWTAuth::fromUser($currentUser);
                 
                 User::where('id', $currentUser->id)
                      ->update(['api_token' => $token]);
                
              } catch (JWTAuthException $e) {
                  return response()->json(['error_code'=>500, 'msg_string'=>'Failed to create token', 'token'=>[]]);
              }
        
              return response()->json(['error_code'=>200, 'msg_string'=>'Token created successful', 'token' => $token]);     

          } else {
              return response()->json(['error_code'=>500, 'msg_string'=>'User not exit, Please signup','result'=>[]]);
          }
        } else {
          $credentials = $request->only('email', 'password');
        }

        
        $token = null;
        try {
           if (!$token = JWTAuth::attempt($credentials)) {
            return response()->json(['error_code'=>422, 'msg_string'=>'Invalid email or password','result'=>[]]);
           } else {
               $user = JWTAuth::toUser($token);
               User::where('id', $user->id)
                    ->update(['api_token' => $token]);
           }
        } catch (JWTAuthException $e) {
            return response()->json(['error_code'=>500, 'msg_string'=>'Failed to create token', 'token'=>[]]);
        }
        
        return response()->json(['error_code'=>200, 'msg_string'=>'Token created successful', 'token' => $token]);

    }


    public function verifyEmail(Request $request){
         $email = $request->only('email');

         $userRow = User::where('email', $email)->get();

         if( count($userRow->toArray()) ) {
             return response()->json(['error_code'=>500, 'msg_string'=>'Email Address is Already Registered', 'result' => false]);
         } else {
             return response()->json(['error_code'=>200, 'msg_string'=>'Email Address Available', 'result' => true]);
         }
    }


    public function forgotPassword(Request $request) {
        $email = $request->only('email', 0);
        $userRowset = User::where('email', $email)->get();
        if( count($userRowset->toArray()) ) {
          foreach($userRowset as $userRow){
            $userRow->update(['remember_token' => md5(microtime())]);
            $userRow->notify(new ForgotPasswordNotification());
          }
          return response()->json(['error_code'=>200, 'msg_string'=>'Please check your registered email for reset link', 'result' => true]);
        } else {
          return response()->json(['error_code'=>500, 'msg_string'=>'Email Address not registered', 'result' => true]);
        }
    }

    public function isValidToken(Request $request) {
        $user = JWTAuth::toUser($request->token);

        if($user) {
          return response()->json(['error_code'=>200, 'msg_string'=> __('messages.valid_token'), 'result' => ['isValid' => true, 'type' => $user->type]]);
        } else {
          return response()->json(['error_code'=>200, 'msg_string'=> __('messages.invalid_token'), 'result' => []]);
        }
    }
    
    public function getAuthUser(Request $request){
        $user = JWTAuth::toUser($request->token);
        return response()->json(['result' => $user]);
    }
}  