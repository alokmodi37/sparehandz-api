<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/
Route::group(['prefix' => 'v1'], function() {
	Route::group(['prefix' => 'auth'], function(){
		Route::post('register', 'UserController@register');
		Route::post('login', 'UserController@login');
		Route::post('verify', 'UserController@verifyEmail');
		Route::post('forgot-password', 'UserController@forgotPassword');
	});

	Route::group(['middleware' => 'jwt.auth'], function () {
	    Route::post('user', 'UserController@getAuthUser')->name('userDetail');
	    Route::any('skills', 'ProfileController@getSkills')->name('skills');
	    
	    Route::post('validate-token', 'UserController@isValidToken');

	    Route::group(['prefix' => 'profile'], function(){
	    	Route::any('get', 'ProfileController@getProfile')->name('getProfile');
	    	Route::any('save', 'ProfileController@saveProfile')->name('saveProfile');
	    	Route::any('setting', 'ProfileController@getSetting')->name('getSetting');
	    	Route::any('saveSetting', 'ProfileController@saveSetting')->name('saveSetting');
	    });

	    Route::group(['prefix' => 'rating'], function(){
	    	Route::any('get', 'RatingController@getRating')->name('getRating');
	    	Route::any('save', 'RatingController@saveRating')->name('saveRating');
	    });

	    Route::group(['prefix' => 'activity'], function(){
	    	Route::any('applied', 'ActivityController@applied')->name('applied');
	    	Route::any('posts', 'ActivityController@posts')->name('posts');
	    	Route::any('pending', 'ActivityController@pending')->name('pending');
	    	Route::any('connection', 'ActivityController@connection')->name('connection');
	    	
	    	Route::any('approveConnection', 'ActivityController@approveConnection')->name('approveConnection');
	    	Route::any('rejectConnection', 'ActivityController@rejectConnection')->name('rejectConnection');
	    });
	    
	});
});
