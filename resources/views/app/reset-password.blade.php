@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reset Password</div>
                <div class="panel-body">
                
                    <div style="color:red">{{$message}}</div>
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('resetPassword', ['token' => $token]) }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if (count($errors) > 0)
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('repassword') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Retype Password</label>

                            <div class="col-md-6">
                                <input id="repassword" type="password" class="form-control" name="repassword" required>

                                @if (count($errors) > 0)
                                    @if ($errors->has('repassword'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('repassword') }}</strong>
                                        </span>
                                    @endif
                                @endif
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Reset Password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection