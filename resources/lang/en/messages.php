<?php

return [
	'valid_token' => 'Provided token is valid',
	'invalid_token' => 'Provided token is invalid'
];