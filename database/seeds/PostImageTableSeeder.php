<?php

use Illuminate\Database\Seeder;

class PostImageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$postRowset = App\Post::all();

    	foreach($postRowset  as $post) {
    		factory(App\PostImage::class, 5)->create(['post_id' =>  $post->id]);
    	}
        
    }
}
