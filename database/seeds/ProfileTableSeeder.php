<?php

use Illuminate\Database\Seeder;

class ProfileTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$profileRowset = App\Profile::all();
    	if( count($profileRowset->toArray()) ){
	    	foreach($profileRowset as $profileRow) {
	    		factory(App\Profile::class)->create(['user_id' => $profileRow->id]);
	    	}
	    } else {
	    	factory(App\Profile::class, 10)->create();
	    }
        
    }
}
