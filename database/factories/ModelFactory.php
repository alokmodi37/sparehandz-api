<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */

/* Helper function to filter id field only */
function fillOnlyFields($rowSet){
	$resultRowSet = [];
	foreach($rowSet as $key => $value){
		foreach($value as $subvalue){
			$resultRowSet[$key] = $subvalue;
		}
	}
	return $resultRowSet;
}

/* Seeder factory for User Table*/
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10)
    ];
});

/* Seeder factory for Profile Table*/
$factory->define(App\Profile::class, function(Faker\Generator $faker, $defaultValues = []) {
	$width = 100;
	$height = 100;

	$users = fillOnlyFields(App\User::select('id')->get()->toArray());
	$skills =   fillOnlyFields(App\Skill::select('id')->get()->toArray());
	
    return array_merge([
        'user_id' => $faker->randomElement($users),
        'skill_id' => $faker->randomElement($skills),
        'bio'  => $faker->sentence, 
        'education'  => $faker->word, 
        'dob' => $faker->date($format = 'Y-m-d', $max = 'now'), 
        'address' => $faker->streetAddress, 
        'latitude' => $faker->latitude($min = -90, $max = 90) , 
        'longitude' => $faker->longitude($min = -180, $max = 180)
    ], $defaultValues);
});

/* Seeder factory for Review Table*/
$factory->define(App\Review::class, function(Faker\Generator $faker) {
	$customer = fillOnlyFields(App\User::select('id')->get()->toArray());
	$provider = fillOnlyFields(App\User::select('id')->get()->toArray());
	
	return [
        'receiver_id' => $faker->randomElement($provider),
        'sender_id' => $faker->randomElement($customer),
        'comment' => $faker->sentence, 
        'rating' => $faker->biasedNumberBetween($min = 1, $max = 5, $function = 'sqrt')
    ];
});

/* Seeder factory for Favourite Table*/
$factory->define(App\Post::class, function(Faker\Generator $faker) {
	$customer = fillOnlyFields(App\User::select('id')->get()->toArray());
	$services =   fillOnlyFields(App\Service::select('id')->get()->toArray());

    $serviceNature = $faker->randomElement(['FREE','FLAT','HOURLY']);

    switch($serviceNature){
        case 'FREE' : $serviceCharge = 0; $serviceDuration = 0; break;

        case 'FLAT' : $serviceCharge = $faker->biasedNumberBetween($min = 10, $max = 500, $function = 'sqrt'); $serviceDuration = 0; break;

        case 'HOURLY' : $serviceCharge = $faker->biasedNumberBetween($min = 10, $max = 500, $function = 'sqrt'); $serviceDuration = $faker->biasedNumberBetween($min = 1, $max = 12, $function = 'sqrt'); break;
    }
	
	return [
        'user_id' => $faker->randomElement($customer),
        'service_type' => $faker->randomElement(['SERVICE', 'SOCIAL', 'PUBLIC']),
        'service_sub_type' => $faker->randomElement($services),
        'caption' => $faker->word,
        'description' => $faker->sentence,
        'service_nature' => $serviceNature,
        'service_duration' => $serviceDuration,
        'service_charge' => $serviceCharge
    ];
});

/* Seeder factory for ServiceImage Table*/
$factory->define(App\PostImage::class, function(Faker\Generator $faker, $defaultValues = [] ) {
    $width = 300;
	$height = 300;

	return array_merge([
        'post_id' => 0,
        'post_image' => $faker->imageUrl($width, $height, 'cats')
    ], $defaultValues);
});


/* Seeder factory for Booking Table*/
$factory->define(App\Booking::class, function(Faker\Generator $faker) {
	$customer = fillOnlyFields(App\User::select('id')->where('type', 'customer')->get()->toArray());
	$provider = fillOnlyFields(App\User::select('id')->where('type', 'provider')->get()->toArray());
	
	return [
        'provider_id' => $faker->randomElement($provider),
        'client_id' => $faker->randomElement($customer),
        'start_date' => $faker->date($format = '2017-m-d', $max = 'now'),
        'start_time' => $faker->time($format = 'H:i:s', $max = 'now'),
        'end_date' => $faker->date($format = '2017-m-d', $max = 'now'),
        'end_time' => $faker->time($format = 'H:i:s', $max = 'now'),
    ];
});
