<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->enum('service_type', ['SERVICE', 'SOCIAL', 'PUBLIC']);

            $table->integer('service_sub_type')->unsigned();
            $table->foreign('service_sub_type')->references('id')->on('services')->onDelete('cascade');

            $table->string('caption');
            $table->string('description');

            $table->enum('service_nature', ['FREE', 'FLAT', 'HOURLY']);
            $table->float('service_charge');
            $table->string('service_duration');

            $table->boolean('is_active')->default(1);
            $table->boolean('is_open')->default(1);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
